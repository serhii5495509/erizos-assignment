import React, { Component } from "react";

class Folder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: !this.isFolderExpanded(),
      isHovered: false,
    };
  }

  toggleCollapse = () => {
    this.setState((prevState) => ({
      collapsed: !prevState.collapsed,
    }));
  };

  handleMouseEnter = () => {
    this.setState({
      isHovered: true,
    });
  };

  handleMouseLeave = () => {
    this.setState({
      isHovered: false,
    });
  };

  isFolderExpanded = () => {
    const { expandedFolders, path } = this.props;
    return expandedFolders.some((folderPath) => path.startsWith(folderPath));
  };

  render() {
    const { name, children } = this.props;
    const { collapsed, isHovered } = this.state;

    return (
      <div>
        <span
          onClick={this.toggleCollapse}
          onMouseEnter={this.handleMouseEnter}
          onMouseLeave={this.handleMouseLeave}
          style={{ cursor: isHovered ? "pointer" : "default" }}
        >
          {collapsed ? "+ " : "- "}
          {name}
        </span>
        {!collapsed && children}
      </div>
    );
  }
}

class File extends Component {
  render() {
    const { name, mime } = this.props;

    return (
      <div>
        {name} ({mime})
      </div>
    );
  }
}

class MyBrowser extends Component {
  renderFolderStructure = (structure, expandedFolders, currentPath = "") => {
    return structure.map((item) => {
      const path = currentPath + "/" + item.name;
      if (item.type === "FOLDER") {
        return (
          <Folder
            key={item.name}
            name={item.name}
            path={path}
            expandedFolders={expandedFolders}
          >
            {this.renderFolderStructure(item.children, expandedFolders, path)}
          </Folder>
        );
      } else {
        return <File key={item.name} name={item.name} mime={item.mime} />;
      }
    });
  };

  render() {
    const { expandedFolders, directoryStructure } = this.props;

    return (
      <div>
        {this.renderFolderStructure(directoryStructure, expandedFolders)}
      </div>
    );
  }
}

export default MyBrowser;

export function myParseInt(string) {
  let result = 0;
  let sign = 1;
  let startIndex = 0;

  while (string[startIndex] === " ") {
    startIndex++;
  }

  if (string[startIndex] === "-") {
    sign = -1;
    startIndex++;
  } else if (string[startIndex] === "+") {
    startIndex++;
  }

  for (let i = startIndex; i < string.length; i++) {
    const digit = string.charCodeAt(i) - 48;
    if (digit >= 0 && digit <= 9) {
      result = result * 10 + digit;
    } else {
      break;
    }
  }

  return sign * result;
}

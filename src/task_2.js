export function printSpiral(matrix) {
  const numRows = matrix.length;
  const numCols = matrix[0].length;

  let top = 0;
  let bottom = numRows - 1;
  let left = 0;
  let right = numCols - 1;

  let direction = 0;

  while (top <= bottom && left <= right) {
    switch (direction % 4) {
      case 0:
        for (let i = left; i <= right; i++) {
          console.log(matrix[top][i]);
        }
        top++;
        break;
      case 1:
        for (let i = top; i <= bottom; i++) {
          console.log(matrix[i][right]);
        }
        right--;
        break;
      case 2:
        for (let i = right; i >= left; i--) {
          console.log(matrix[bottom][i]);
        }
        bottom--;
        break;
      case 3:
        for (let i = bottom; i >= top; i--) {
          console.log(matrix[i][left]);
        }
        left++;
        break;
      default:
        break;
    }
    direction++;
  }
}

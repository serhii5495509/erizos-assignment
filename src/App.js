import MyBrowser from "./Task_3";

import data from "./data.json";

import "./App.css";

const expandedFolders = ["/Common7", "/Common7/Tools", "/Common7/IDE"];

function App() {
  return (
    <div className="App">
      <MyBrowser expandedFolders={expandedFolders} directoryStructure={data} />
    </div>
  );
}

export default App;
